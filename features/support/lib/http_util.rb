require 'uri'
require 'rest-client'

require_relative './auth_util'
require_relative './config'

# Various utility functions related to authentication used for testing.
module HTTPUtils

  # Makes a GET HTTP request to the REST api.
  #
  # @param endpoint [String] The endpoint on the REST api to call.
  # @return [Object] The response from the GET HTTP request.
  def self.get_api_request(endpoint)
    url = URI.join(Config['baseUrl'], endpoint).to_s
    Cucumber.logger.debug("Making a GET request to #{url}\n")
    RestClient.get(url, get_authentication_header(url))
  end

  # Makes a POST HTTP request to the REST api.
  #
  # @param endpoint [String] The endpoint on the REST api to call.
  # @param body [Object] The body of the POST request.
  # @return [Object] The response from the POST HTTP request.
  def self.post_api_request(endpoint, body)
    url = URI.join(Config['baseUrl'], endpoint).to_s
    Cucumber.logger.debug("Making a POST request to #{url}\n")
    RestClient.post(url, body, AuthUtils.post_authentication_header(url, body.to_s))
  end

  # Makes a PUT HTTP request to the REST api.
  #
  # @param endpoint [String] The endpoint on the REST api to call.
  # @param body [Object] The body of the POST request.
  # @return [Object] The response from the PUT HTTP request.
  def self.put_api_request(endpoint, body)
    url = URI.join(Config['baseUrl'], endpoint).to_s
    Cucumber.logger.debug("Making a PUT request to #{url}\n")
    RestClient.put(url, body, AuthUtils.post_authentication_header(url, body.to_s))
  end

  # Makes a DELETE HTTP request to the REST api.
  #
  # @param endpoint [String] The endpoint on the REST api to call.
  # @return [Object] The response from the DELETE HTTP request.
  def self.delete_api_request(endpoint)
    url = URI.join(Config['baseUrl'], endpoint).to_s
    Cucumber.logger.debug("Making a DELETE request to #{url}\n")
    RestClient.delete(url, AuthUtils.get_authentication_header(url))
  end

end
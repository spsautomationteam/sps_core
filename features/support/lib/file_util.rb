# Various utility functions related to files used for testing
module FileUtils

  # Retrieves a test data file and parses it as XML.
  #
  # @param file_name [String] The name of the file (without the extension) in the `test_data`
  #   folder. If the string ends in `'.xml'` it assumes that the string is a path to the file.
  # @return [Nokogiri::XML] The content of the test data file parsed as XML.
  def self.get_test_data_file(file_name)
    path = if file_name.end_with?('.xml')
             file_name
           else
             File.join('test_data', file_name + '.xml')
           end
    raise "Test data file #{path} was not found" unless File.exist?(path)
    Cucumber.logger.debug("Loading test data file #{path}\n")
    xml = File.open(path) { |f| Nokogiri::XML(f) }
    if xml.errors.any?
      raise "The XML in file #{path} resulted in errors while parsing it:\n
            #{xml.errors.join("\n")}"
    end
    xml
  end

end
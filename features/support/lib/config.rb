require 'yaml'

# Loads the testing config.
module Config

  # The base YAML config object.
  base_config = YAML.safe_load(File.read('config/config.yaml'))

  # The name of the profile to load.
  #
  # @return [String] The name of the profile to load.
  def self.profile_name
    ENV['PROFILE_NAME'] || 'default'
  end

  # The merged YAML config used for testing.
  @config = base_config['default'].merge(base_config[profile_name])

  # Gets a key from the config object.
  #
  # @param key [String] The key of the object to get from the config object.
  # @return [Object] The value stored at `key` in the config object.
  def self.[](key)
    @config[key]
  end

  # Sets the value of a key on the config object.
  #
  # @param key [String] The key of the object on the config object to replace.
  # @param value [Object] The value to override the current value with.
  def self.[]=(key, value)
    @config[key] = value
  end

end
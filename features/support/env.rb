require_relative './lib/auth_util'
require_relative './lib/config'
require_relative './lib/file_util'
require_relative './lib/http_util'

Cucumber.logger.info("Testing with profile '#{Config.profile_name}'\n")
Cucumber.logger.level = Logger::DEBUG if Config['debug']

World(AuthUtils, Config, FileUtils, HTTPUtils)

require 'nokogiri'

When(/^I make a (.*) request to (\S*)(?: with test data (\S*))?$/) do |method, endpoint, test_data|
  @body = if test_data.nil? || test_data.to_s.empty?
            nil
          else
            FileUtils.get_test_data_file(test_data.to_s)
          end
  @response = begin
    case method.to_s.upcase
    when 'GET'
      HTTPUtils.get_api_request(endpoint)
    when 'POST'
      HTTPUtils.post_api_request(endpoint, @body)
    when 'PUT'
      HTTPUtils.put_api_request(endpoint, @body)
    when 'DELETE'
      HTTPUtils.delete_api_request(endpoint)
    else
      raise "Unknown HTTP method #{method}"
    end
  rescue RestClient::RequestFailed => e
    raise e, 'Error occurred while making request' if e.response.nil?
    e.response
  end
end

Then(/^The response code should be (.*)$/) do |expected_code|
  expect(@response.code).to eq(expected_code.to_i)
end

Then(/^The response body should( not)? be empty$/) do |not_found|
  if not_found.nil?
    expect(@response.body).to be_empty
  else
    expect(@response.body).not_to be_empty
  end
end
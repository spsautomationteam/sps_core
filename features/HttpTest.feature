Feature: HttpTest
  Random test to make sure things work.

  Scenario Outline: making a GET Request
    When I make a <method> request to frmpayment.aspx with test data <test_data>
    Then The response code should be 200
    Then The response body should be empty
    Examples:
      | method | test_data |
      | POST   | TC_1      |
# SPS Core Tests

## How to Run
```bash
bundle install
cucumber --color -r features
```
**OR**

```bash
bundle install
rake cucumber
```

## Running Tests With Different Profiles
When loading the config the tests look at the `PROFILE_NAME` environment variable to determine what profile to load and 
then that profile is then _merged_ with the `default` profile to get final configuration object that is used during 
testing. If the `PROFILE_NAME` environment variable is not found then only the `default` configuration profile is looked
at.

## Generating Documentation
```bash
yard /features/support/**/*.rb
```
**OR**

```bash
rake yard
```